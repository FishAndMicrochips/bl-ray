;;;; bl-ray.asd
;;
;;;; Copyright (c) 2021 Blueberry. See LICENSE for details


(asdf:defsystem #:bl-ray
  :description "Following Alexander Lehmann's tutorial on ray tracing"
  :author "Blueberry"
  :license  "GPL V3"
  :version "0.0.1"
  :serial t
  :depends-on (#:cl #:cl-ppcre #:zpng #:rutils #:alexandria)
  :components ((:file "package")
               (:file "bl-ray")))
